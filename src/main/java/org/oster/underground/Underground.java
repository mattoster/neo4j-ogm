package org.oster.underground;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.oster.underground.entity.Connection;
import org.oster.underground.entity.StationNode;
import org.oster.underground.model.Station;
import org.oster.underground.properties.GraphLog;
import org.oster.underground.properties.GraphProps;
import org.oster.underground.utils.Create;
import org.oster.underground.utils.Parse;
import org.oster.underground.utils.Part;
import org.oster.underground.utils.Translator;

public class Underground {

    public static void main(String[] args) {
        Logger logger = GraphLog.getLogger(Underground.class);
        logger.setLevel(Level.INFO);

        Parse parser = new Parse(GraphProps.getApplicationProperty("underground.dataFile"));

        Translator translator = new Translator();

        // Parse the data
        logger.info("Starting parse of underground data...");
        parser.readLines();
        List<Part> parts = parser.setLineParts();
        logger.log(Level.INFO, "=========[ Parsed {0} lines. ]=========", parts.size());

        // Organize data into a structure
        Create create = new Create(parts);
        List<Station> stations = create.getStationsFromParts();
        logger.log(Level.INFO, "=========[Organized {0} stations. ]=========", stations.size());

        // Translate from parse entities into Nodes and relationships
        // TODO: create line nodes, zone nodes, and station nodes
        List<StationNode> stationNodes = new ArrayList<>();
        for (Station station : stations) {
            StationNode stationNode = translator.entityFromModel(station);
            stationNodes.add(stationNode);
        }
        logger.log(Level.INFO, "=========[Re-organized {0} stations into nodes. ]=========", stationNodes.size());

        // Station node structure exists. Need to persist stations first
        // Line and Zone nodes can also be made
        for (StationNode stationNode : stationNodes) {
            translator.persistStation(stationNode, false);
        }

        logger.log(Level.INFO, "=========[Persisted {0} stations into graph. ]=========", stationNodes.size());

        // Station nodes exist, now set and persist connections between stations
        int numConnections = 0;
        for (StationNode stationNode : stationNodes) {
            stationNode.setConnections(translator.getConnectionsFromStation(stationNode));
            numConnections += stationNode.getConnections().size();
            // translator.persistStation(stationNode, true);
            for (Connection connection : stationNode.getConnections()) {
                translator.persistConnection(connection);
            }

//            StationNode tmp = translator.fetchStation(stationNode.getStationId());
//            if(stationNode.getConnections().size() != tmp.getConnections().size()) {
//                logger.severe(String.format("Expected to persist station %d with %d connections but was %d",
//                                            stationNode.getStationId(), stationNode.getConnections().size(),
//                                            tmp.getConnections().size()));
//            }
        }

        String msg = String.format("Created/Updated %d nodes and %d connections. Done. %s", stationNodes.size(),
                                   numConnections, stationNodes.toString());
        logger.log(Level.INFO, msg);
    }

}
