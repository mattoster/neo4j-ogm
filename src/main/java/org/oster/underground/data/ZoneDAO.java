package org.oster.underground.data;

import java.util.Collections;
import java.util.logging.Logger;
import org.oster.underground.entity.ZoneNode;
import org.oster.underground.properties.GraphLog;

public class ZoneDAO extends BaseDAO<ZoneNode> {
    private static final Logger LOGGER = GraphLog.getLogger(ZoneDAO.class);

    public ZoneNode findZoneNodeByZoneId(Long id) {
        getLogger().info("called: findZoneNodeByZoneId() on " + getCurrentDateString());
        ZoneNode result = null;
        String query = String.format("MATCH (zone:Zone) WHERE zone.nodeId = %d RETURN zone;", id);
        Iterable<ZoneNode> results = getSession().query(ZoneNode.class, query, Collections.emptyMap());

        if (results != null && results.iterator().hasNext()) {
            result = results.iterator().next();
            getLogger().info(String.format("Query: '%s'\n\tResult: %s", query, result.toString()));
        }
        getLogger().info("exited: findZoneNodeByZoneId() on " + getCurrentDateString());
        return result;
    }

    public ZoneNode findZoneByName(String name) {
        getLogger().info("called: findZoneByName() on " + getCurrentDateString());
        ZoneNode result = null;
        String query = String.format("MATCH (zone:Zone) WHERE zone.name = '%s' RETURN zone;", name);
        Iterable<ZoneNode> results = getSession().query(ZoneNode.class, query, Collections.emptyMap());

        if (results != null && results.iterator().hasNext()) {
            result = results.iterator().next();
            getLogger().info(String.format("Query: '%s'\n\tResult: %s", query, result.toString()));
        }
        getLogger().info("exited: findZoneByName() on " + getCurrentDateString());
        return result;
    }

    public void createConstraints() {
        String query = "CREATE CONSTRAINT ON (n:Zone) ASSERT n.nodeId IS UNIQUE";
        getSession().query(query, Collections.emptyMap());
    }

    @Override
    public Class<ZoneNode> getEntityType() {
        return ZoneNode.class;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }

}
