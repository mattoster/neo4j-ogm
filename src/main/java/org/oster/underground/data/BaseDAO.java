package org.oster.underground.data;

import java.util.Date;
import java.util.logging.Logger;
import org.neo4j.ogm.session.Session;
import org.oster.underground.entity.Node;

public abstract class BaseDAO<T extends Node> {
    private static final int DEPTH_ALL = -1;
    private static final int DEPTH_ENTITY = 1;
    private static final int DEPTH_LIST = 0;

    private Session session = Neo4jSessionFactory.getInstance().getNeo4jSession();

    public BaseDAO() {

    }

    public Iterable<T> findAll() {
        getLogger().info("called: findAll() on " + getCurrentDateString());
        Iterable<T> results = session.loadAll(getEntityType(), DEPTH_LIST);
        getLogger().info("exited: findAll() on " + getCurrentDateString());
        return results;
    }

    public T find(Long id) {
        getLogger().info("called: find() on " + getCurrentDateString());
        T result = session.load(getEntityType(), id, DEPTH_ENTITY);
        getLogger().info("exited: find() on " + getCurrentDateString());
        return result;
    }

    public void delete(Long id) {
        getLogger().info("called: delete() on " + getCurrentDateString());
        session.delete(session.load(getEntityType(), id));
        getLogger().info("exited: delete() on " + getCurrentDateString());
    }

    public void createOrUpdate(T entity) {
        String msg = String.format("createOrUpdate(%s)", entity.toString());
        getLogger().info(msg + " entered on " + getCurrentDateString());
        createOrUpdate(entity, DEPTH_ALL);
        getLogger().info(msg + "exited on " + getCurrentDateString());
    }

    public void createOrUpdateImmediate(T entity) {
        String msg = String.format("createOrUpdate(%s)", entity.toString());
        getLogger().info(msg + " entered on " + getCurrentDateString());
        createOrUpdate(entity, DEPTH_ENTITY);
        getLogger().info(msg + "exited on " + getCurrentDateString());
    }

    private void createOrUpdate(T entity, int depth) {
        String msg = String.format("createOrUpdate(%s, %d)", entity.toString(), depth);
        getLogger().info(msg + " entered on " + getCurrentDateString());
        getLogger().info("called: createOrUpdate(" + depth + ") on " + getCurrentDateString());
        try {
            session.save(entity, depth);
        } catch (Exception e) {
            String error = String.format("Could not create/update node: %s", entity.toString());
            getLogger().severe(error);
        }
        getLogger().info(msg + "exited on " + getCurrentDateString());
    }

    public Session getSession() {
        return session;
    }
    
    String getCurrentDateString() {
        Date date = new Date(System.currentTimeMillis());
        return date.toString();
    }

    public abstract Class<T> getEntityType();

    public abstract Logger getLogger();

}
