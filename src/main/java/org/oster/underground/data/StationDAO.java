package org.oster.underground.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import org.oster.underground.entity.Connection;
import org.oster.underground.entity.StationNode;
import org.oster.underground.properties.GraphLog;

public class StationDAO extends BaseDAO<StationNode> {
    private static final Logger LOGGER = GraphLog.getLogger(StationDAO.class);

    public StationDAO() {

    }

    public StationNode findStationByStationId(Long id) {
        String msg = String.format("findStationByStationId(%d) entered on %s...", id, getCurrentDateString());
        getLogger().info(msg);
        StationNode result = null;
        String query = String.format("match (s:Station {nodeId: %s}) return s", id);
        Iterable<StationNode> results = getSession().query(StationNode.class, query, Collections.emptyMap());

        if (results != null && results.iterator().hasNext()) {
            result = results.iterator().next();
            getLogger().info(String.format("Query: '%s'\n\tResult: %s", query, result.toString()));
        }
        msg = String.format("findStationByStationId(%d) exited on %s...", id, getCurrentDateString());
        getLogger().info(msg);
        return result;
    }

    public List<StationNode> findStationsByName(String name) {
        getLogger().info("called: findStationsByName() on " + getCurrentDateString());
        List<StationNode> matchingNodes = new ArrayList<>();
        String query = String.format("MATCH (station:Station) WHERE station.name = '%s' RETURN station;", name);
        String regExQuery = String.format("MATCH (station:Station) WHERE s.name =~ '(?i)%s.*' RETURN station;", name);
        Iterable<StationNode> results = getSession().query(StationNode.class, query, Collections.emptyMap());

        if (results != null && results.iterator().hasNext()) {
            getLogger().info(String.format("Query: '%s'\n", query));
            for(StationNode station : results) {
                matchingNodes.add(station);
                getLogger().info(String.format("\tResult: %s\n", station.toString()));
            }
        }
        getLogger().info("exited: findStationsByName() on " + getCurrentDateString());
        return matchingNodes;
    }

    public void addConnection(Connection connection) {
        Long s1Id = connection.getStartNode().getStationId();
        Long s2Id = connection.getEndNode().getStationId();
        String msg = String.format("findStationByStationId(%s, %s) entered on %s...", s1Id, s2Id,
                                   getCurrentDateString());
        getLogger().info(msg);

        if(s1Id.equals(s2Id)) {
            String selfMsg = String.format("Station %s created a self connection %s.",
                                           connection.getStartNode().toString(), connection.toString());
            getLogger().severe(selfMsg);
        }

        // Set up the query
        String match = String.format("match (s1:Station {nodeId: %s}), (s2:Station {nodeId: %s})",
                                     s1Id, s2Id);
        String props = String.format("{line: '%s'}", connection.getConnectionLine());
        String create = String.format("merge (s1)-[:Connects %s]->(s2)", props);
        String query = String.format("%s %s", match, create);

        Object obj = getSession().query(StationNode.class, query, Collections.emptyMap());
    }

    public void createConstraints() {
        String query = "CREATE CONSTRAINT ON (n:Station) ASSERT n.nodeId IS UNIQUE";
        getSession().query(query, Collections.emptyMap());
    }

    @Override
    public Class<StationNode> getEntityType() {
        return StationNode.class;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }

}
