package org.oster.underground.data;

import java.util.Collections;
import java.util.logging.Logger;
import org.oster.underground.entity.LineNode;
import org.oster.underground.properties.GraphLog;

public class LineDAO extends BaseDAO<LineNode> {
    private static final Logger LOGGER = GraphLog.getLogger(LineDAO.class);

    public LineNode findLineNodeByLineId(Long id) {
        getLogger().info("called: findLineNodeByLineId() on " + getCurrentDateString());
        LineNode result = null;
        String query = String.format("MATCH (line:Line) WHERE line.nodeId = %d RETURN line;", id);
        Iterable<LineNode> results = getSession().query(LineNode.class, query, Collections.emptyMap());

        if (results != null && results.iterator().hasNext()) {
            result = results.iterator().next();
            getLogger().info(String.format("Query: '%s'\n\tResult: %s", query, result.toString()));
        }
        getLogger().info("exited: findLineNodeByLineId() on " + getCurrentDateString());
        return result;
    }

    public LineNode findLineByName(String name) {
        getLogger().info("called: findLineByName() on " + getCurrentDateString());
        LineNode result = null;
        String query = String.format("MATCH (line:Line) WHERE line.name = %s RETURN line;", name);
        Iterable<LineNode> results = getSession().query(LineNode.class, query, Collections.emptyMap());

        if (results != null && results.iterator().hasNext()) {
            result = results.iterator().next();
            getLogger().info(String.format("Query: '%s'\n\tResult: %s", query, result.toString()));
        }
        getLogger().info("exited: findLineByName() on " + getCurrentDateString());
        return result;
    }

    public void createConstraints() {
        String query = "CREATE CONSTRAINT ON (n:Line) ASSERT n.nodeId IS UNIQUE";
        getSession().query(query, Collections.emptyMap());
    }

    @Override
    public Class<LineNode> getEntityType() {
        return LineNode.class;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }

}
