package org.oster.underground.service;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.neo4j.ogm.transaction.Transaction;
import org.oster.underground.data.LineDAO;
import org.oster.underground.entity.LineNode;
import org.oster.underground.properties.GraphLog;

public class LineService {
    private static final Logger LOGGER = GraphLog.getLogger(LineService.class);

    private final LineDAO lineDAO;

    public LineService() {
        lineDAO = new LineDAO();
        this.createConstraints();
    }

    public void createOrUpdateLine(LineNode line) {
        try (Transaction tx = lineDAO.getSession().beginTransaction()) {
            lineDAO.createOrUpdate(line);
            tx.commit();
            LOGGER.log(Level.INFO, "Created Line {0}", line.toString());
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while creating line: ", e);
        }
    }

    public LineNode getLineByLineId(Long id) {
        LineNode node = null;
        try (Transaction tx = lineDAO.getSession().beginTransaction()){
            node = lineDAO.findLineNodeByLineId(id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error getting line by line id: ", e);
        }
        return node;
    }

    private void createConstraints() {
        try (Transaction tx = lineDAO.getSession().beginTransaction()){
            lineDAO.createConstraints();
            tx.commit();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error creating constraints: ", e);
        }
    }

}
