package org.oster.underground.service;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.neo4j.ogm.transaction.Transaction;
import org.oster.underground.data.ZoneDAO;
import org.oster.underground.entity.ZoneNode;
import org.oster.underground.properties.GraphLog;

public class ZoneService {
    private static final Logger LOGGER = GraphLog.getLogger(ZoneService.class);

    private final ZoneDAO zoneDAO;

    public ZoneService() {
        zoneDAO = new ZoneDAO();
        this.createConstraints();
    }

    public void createOrUpdateZone(ZoneNode zone) {
        try (Transaction tx = zoneDAO.getSession().beginTransaction()) {
            zoneDAO.createOrUpdate(zone);
            tx.commit();
            LOGGER.log(Level.INFO, "Created Zone {0}", zone.toString());
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while creating zone: ", e);
        }
    }

    public ZoneNode getZoneByZoneId(Long id) {
        ZoneNode node = null;
        try (Transaction tx = zoneDAO.getSession().beginTransaction()){
            node = zoneDAO.findZoneNodeByZoneId(id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error getting zone by zone id: ", e);
        }
        return node;
    }

    private void createConstraints() {
        try (Transaction tx = zoneDAO.getSession().beginTransaction()){
            zoneDAO.createConstraints();
            tx.commit();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error creating constraints: ", e);
        }
    }

}
