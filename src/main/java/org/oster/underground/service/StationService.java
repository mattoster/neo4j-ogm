package org.oster.underground.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.neo4j.ogm.transaction.Transaction;
import org.oster.underground.data.StationDAO;
import org.oster.underground.entity.Connection;
import org.oster.underground.entity.StationNode;
import org.oster.underground.properties.GraphLog;

public class StationService {
    private static final Logger LOGGER = GraphLog.getLogger(StationService.class);
    private StationDAO stationDAO;

    public StationService() {
        stationDAO = new StationDAO();
        this.createConstraints();
    }

    public void createOrUpdateStation(StationNode station) {
        try (Transaction tx = stationDAO.getSession().beginTransaction()) {
            stationDAO.createOrUpdate(station);
            tx.commit();
            LOGGER.log(Level.INFO, "Created Station {0}", station.toString());
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while creating station: ", e);
        }
    }

    public void addConnectionBetweenStations(Connection connection) {
        try(Transaction tx = stationDAO.getSession().beginTransaction()) {
            stationDAO.addConnection(connection);
            tx.commit();
            LOGGER.log(Level.INFO, "Created connection {0}", connection.toString());
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error while adding connection: ", e);
        }
    }

    public StationNode getStationById(Long id) {
        StationNode station = null;

        try (Transaction tx = stationDAO.getSession().beginTransaction()) {
            station = stationDAO.find(id);
            tx.commit();
            if (station == null) {
                LOGGER.log(Level.INFO, "Station with id {0} not found.", id);
            }
            else {
                LOGGER.log(Level.INFO, "Found station: {0}", station.toString());
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error finding station by id: ", e);
        }

        return station;
    }

    public StationNode getStationByStationId(Long id) {
        StationNode station = null;
        try (Transaction tx = stationDAO.getSession().beginTransaction()){
            station = stationDAO.findStationByStationId(id);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error getting station by station id: ", e);
        }
        return station;
    }

    public List<StationNode> getStationsByName(String name) {
        List<StationNode> stations = new ArrayList<>();
        try (Transaction tx = stationDAO.getSession().beginTransaction()){
            stations = stationDAO.findStationsByName(name);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error getting stations by name: ", e);
        }
        return stations;
    }

    private void createConstraints() {
        try (Transaction tx = stationDAO.getSession().beginTransaction()){
            stationDAO.createConstraints();
            tx.commit();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error creating constraints: ", e);
        }
    }

}
