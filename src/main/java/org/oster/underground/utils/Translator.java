package org.oster.underground.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.collections4.map.HashedMap;
import org.oster.underground.entity.Connection;
import org.oster.underground.entity.LineNode;
import org.oster.underground.entity.StationNode;
import org.oster.underground.entity.ZoneNode;
import org.oster.underground.model.Conn;
import org.oster.underground.model.Line;
import org.oster.underground.model.Station;
import org.oster.underground.model.Zone;
import org.oster.underground.properties.GraphLog;
import org.oster.underground.service.LineService;
import org.oster.underground.service.StationService;
import org.oster.underground.service.ZoneService;

public class Translator {
    private static final Logger LOGGER = GraphLog.getLogger(Translator.class);

    private Map<Long, LineNode> lineNodes;
    private Map<Long, ZoneNode> zoneNodes;

    private Map<Long, Station> parsedStations;

    private LineService lineService;
    private ZoneService zoneService;
    private StationService stationService;

    public Translator() {
        lineService = new LineService();
        zoneService = new ZoneService();
        stationService = new StationService();

        lineNodes = this.getLineNodeMap();
        zoneNodes = this.getZoneNodeMap();
        parsedStations = new HashMap<>();
    }

    public StationNode entityFromModel(Station station) {
        StationNode node = null;
        if (station != null) {
            node = new StationNode(station.getId(), station.getName());
            node.setLines(getLineNodesFromLines(station.getLines()));
            node.setZones(getZoneNodesFromZones(station.getZones()));
            node.setAccessibleLines(station.getAccessibleLines());
            node.setTerminusLines(station.getTerminusLines());
            parsedStations.put(station.getId(), station);
        }
        return node;
    }

    private Map<Long, LineNode> getLineNodeMap() {
        Map<Long, LineNode> lines = new HashedMap<>();
        for (Line line : Line.values()) {
            LineNode node = new LineNode(line.getLineId(), line.getLineName());

            LineNode found = lineService.getLineByLineId(line.getLineId());
            if (found == null) {
                lineService.createOrUpdateLine(node);
                lines.put(node.getLineId(), node);
            }
            else {
                lines.put(found.getLineId(), found);
            }
        }
        return lines;
    }

    private Map<Long, ZoneNode> getZoneNodeMap() {
        Map<Long, ZoneNode> zones = new HashedMap<>();
        for (Zone zone : Zone.values()) {
            ZoneNode node = new ZoneNode(zone.getZoneId(), zone.getZoneName());

            ZoneNode found = zoneService.getZoneByZoneId(zone.getZoneId());
            if (found == null) {
                zoneService.createOrUpdateZone(node);
                zones.put(node.getZoneId(), node);
            }
            else {
                zones.put(found.getZoneId(), found);
            }
        }
        return zones;
    }

    private List<LineNode> getLineNodesFromLines(List<Line> lines) {
        List<LineNode> nodes = new ArrayList<>();
        if (lines != null) {
            for (Line line : lines) {
                LineNode tmp = lineNodes.get(line.getLineId());
                if (tmp != null) {
                    nodes.add(tmp);
                }
            }
        }
        return nodes;
    }

    private List<ZoneNode> getZoneNodesFromZones(List<Zone> zones) {
        List<ZoneNode> nodes = new ArrayList<>();
        if (zones != null) {
            for (Zone zone : zones) {
                ZoneNode tmp = zoneNodes.get(zone.getZoneId());
                if (tmp != null) {
                    nodes.add(tmp);
                }
            }
        }
        return nodes;
    }

    public List<Connection> getConnectionsFromStation(StationNode startNode) {
        LOGGER.info("Working on station: " + startNode.getStationId() + " - " + startNode.getName());
        Station station = parsedStations.get(startNode.getStationId());
        List<Connection> connections = new ArrayList<>();
        for (Conn conn : station.getConnections()) {
            StationNode endNode = getExistingOrNewNode(conn.getStation().getId(), conn.getStation().getName());
            Connection connection = new Connection(startNode, endNode, conn.getLine().getLineName());
            connections.add(connection);
        }
        return connections;
    }

    public StationNode fetchStation(Long stationId) {
        return stationService.getStationByStationId(stationId);
    }

    public void persistStation(StationNode stationNode, boolean isUpdateAllowed) {
        // Persist station data into Neo4j
        StationNode found = fetchStation(stationNode.getStationId());
        if (found == null) {
            // Station doesn't exist? persist the station.
            persistStationNode(stationNode);
        }
        else {
            if (isUpdateAllowed) {
                // Station exists and the application allows updating? update the station.
                stationNode.setId(found.getId());
                persistStationNode(stationNode);
            }
            else {
                LOGGER.log(Level.INFO, "Station {0} exists and updates not allowed, nothing to do.",
                           stationNode.toString());
            }
        }
    }

    public void persistConnection(Connection connection) {
        try {
            stationService.addConnectionBetweenStations(connection);
        } catch (Exception e) {
            String msg = String.format("Could not create connection %s -> %s: %s",
                                       connection.getStartNode().getStationId(), connection.getEndNode().getStationId(),
                                       e.getMessage());
            LOGGER.info(msg);
        }
    }

    private void persistStationNode(StationNode station) {
        try {
            stationService.createOrUpdateStation(station);
        } catch (Exception e) {
            String msg = String.format("Could not create/update station %d - %s: %s", station.getStationId(),
                                       station.getName(), e.getMessage());
            LOGGER.info(msg);
        }
    }

    private StationNode getExistingOrNewNode(Long stationId, String name) {
        String msg = String.format("getExistingOrNewNode(%d, %s) entered...", stationId, name);
        LOGGER.info(msg);
        StationNode node = fetchStation(stationId);
        if (node == null) {
            node = new StationNode(stationId, name);
        }
        msg = String.format("getExistingOrNewNode(%d, %s) exited...", stationId, name);
        LOGGER.info(msg);
        return node;
    }

}
