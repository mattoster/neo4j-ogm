package org.oster.underground.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.oster.underground.properties.GraphLog;

public class Parse {
    private static final Logger LOGGER = GraphLog.getLogger(Parse.class);

    private final String DELIMITER = ",";
    private final String INTERNAL_DELIMITER = ";";
    
    private String filename;
    private List<String> lines = new ArrayList<>();
    private List<Part> parts = new ArrayList<>();

    public Parse(String filename) {
        this.filename = filename;
    }

    public void readLines() {
        LOGGER.info("Reading lines from the data file: " + this.filename);
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(this.filename))) {
            String line = reader.readLine();
            while (line != null) {
                this.lines.add(line);
                line = reader.readLine();
            }
        } catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, "Error while reading lines from file: " + this.filename, ioe);
        }
        LOGGER.info("Finished reading lines.");
    }

    public List<Part> setLineParts() {
        LOGGER.info("Splitting lines into their parts...");
        if (lines.isEmpty()) {
            LOGGER.log(Level.WARNING, "No lines found. Was File parsed?");
        }
        else {
            String header = lines.get(0);
            String[] headerParts = {};
            for (String line : this.lines) {
                Part part = new Part();
                String[] lineParts = splitIntoParts(line);
                
                if(header != null && header.compareTo(line) == 0) {
                    LOGGER.info("Using this line as a header: " + header);
                    headerParts = header.trim().split(DELIMITER);
                }
                else {
                    if (lineParts.length == headerParts.length) {
                        for (int i = 0; i < lineParts.length; i++) {
                            String head = headerParts[i];
                            String value = lineParts[i];
                            String[] params = {head, value};
                            try {
                                switch (head.toLowerCase()) {
                                    case "id":
                                        part.setId(new Long(value));
                                        break;
                                    case "name":
                                        part.setName(value);
                                        break;
                                    case "lines":
                                        part.setLines(value.trim().split(INTERNAL_DELIMITER));
                                        break;
                                    case "zones":
                                        part.setZones(value.trim().split(INTERNAL_DELIMITER));
                                        break;
                                    case "connections":
                                        part.setConnections(value.trim().split(INTERNAL_DELIMITER));
                                        break;
                                    case "accessibility":
                                        part.setAccessibility(value.trim().split(INTERNAL_DELIMITER));
                                        break;
                                    case "terminus":
                                        part.setTerminus(value.trim().split(INTERNAL_DELIMITER));
                                        break;
                                    default:
                                        LOGGER.log(Level.WARNING, "The header component \"{0}\" cannot be handled.",
                                                   head);
                                }
                            } catch (Exception e) {
                                LOGGER.log(Level.SEVERE, "Could not parse component {0} with a value of {1}: ", params);
                            }
                        }
                        this.parts.add(part);
                    }
                    else {
                        LOGGER.log(Level.WARNING, "Line does not have enough components: {0}", line);
                    }
                }
            }
        }
        LOGGER.info("Finished gathering parts");
        return getParts();
    }

    private String[] splitIntoParts(String line) {
        String[] lineParts;
        List<Integer> positions = new ArrayList<>();

        if(line.contains("\"")) {
            int occur = 0;
            for(int i = 0; i < line.length(); i++) {
                if (line.charAt(i) == '\"') {
                    positions.add(i);
                    occur++;
                }
            }
            if(occur % 2 == 0) {
                for(int i = 0; i < positions.size(); i++){
                    int start = positions.get(i);
                    int end = positions.get(++i);
                    int cur = start;
                    while (cur != end) {
                        if(line.charAt(cur) == ',') {
                            line = line.substring(0, cur) + "|" + line.substring(cur + 1);
                        }
                        cur++;
                    }
                }
                line = line.replace("\"", "");
            }
            else {
                LOGGER.log(Level.SEVERE, "Quotes must be balanced, {0} does not conform!", line);
            }
            lineParts = line.trim().split(DELIMITER);
            for(int i = 0; i < lineParts.length; i++){
                lineParts[i] = lineParts[i].replace("|", ",");
            }
        }
        else {
            lineParts = line.trim().split(DELIMITER);
        }
        return lineParts;
    }

    public List<String> getLines() {
        return lines;
    }

    public List<Part> getParts() {
        return parts;
    }
}
