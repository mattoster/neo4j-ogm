package org.oster.underground.utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.oster.underground.model.Line;
import org.oster.underground.model.Station;
import org.oster.underground.model.Zone;
import org.oster.underground.properties.GraphLog;

public class Create {
    private static final Logger LOGGER = GraphLog.getLogger(Create.class);
    private final List<Part> parts;
    private Map<Long, Station> stations = new HashMap<>();

    public Create(List<Part> parts) {
        this.parts = parts;
        for (Part part : this.parts) {
            Station station = new Station(part.getId(), part.getName());
            stations.put(station.getId(), station);
        }
    }

    public List<Station> getStationsFromParts() {
        for (Part part : getParts()) {
            Station station = new Station(part.getId(), part.getName());
            this.addLines(station, part);
            this.addZones(station, part);
            this.addConnections(station, part);

            this.addAccessibility(station, part);
            this.addTerminus(station, part);

            if (stations.containsKey(station.getId())) {
                LOGGER.log(Level.INFO, "Station with an ID of {0} is already in the stations list!",
                           station.getId());
                Station existing = stations.get(station.getId());
                if (station.getLines().size() > existing.getLines().size() ||
                    station.getZones().size() > existing.getZones().size() ||
                    station.getConnections().size() > existing.getConnections().size()) {
                    stations.remove(station.getId());
                    stations.put(station.getId(), station);
                }
            }
            else {
                LOGGER.log(Level.INFO, "Station with an ID of {0} was added to the stations list!", station.getId());
                stations.put(station.getId(), station);
            }
        }
        return new ArrayList<>(stations.values());
    }

    private List<Part> getParts() {
        return this.parts;
    }

    private void addLines(Station station, Part part) {
        String[] lines = part.getLines();
        for (String line : lines) {
            try {
                Line lineEnum = Line.getLineEnumByLineName(line);
                station.addLine(lineEnum);
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Could not find a Line enum for {0}!", line);
            }
        }
    }

    private void addZones(Station station, Part part) {
        String[] zones = part.getZones();
        for (String zone : zones) {
            try {
                String toEnum = numToString(zone);
                Zone zoneEnum = Zone.valueOf(toEnum);
                station.addZone(zoneEnum);
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Could not find a Zone enum for {0}!", zone);
            }
        }
    }

    private void addConnections(Station station, Part part) {
        String[] connections = part.getConnections();
        for (String connection : connections) {
            String[] connectionParts = connection.replace(";", "").trim()
                                                 .replace("\\)", "").split("\\(");
            boolean hasParts = connectionParts.length > 1;
            if (hasParts) {
                String stationId = connectionParts[0];
                String lineString = connectionParts[1];
                if(lineString.toLowerCase().contains("one-way")) {
                    handleOneWay(stationId, lineString, station, part, connectionParts);
                }
                else {
                    handleWithLine(stationId, lineString, station, part, false);
                }
            }
            else {
                handleNoLine(station, part, connectionParts, false);
            }
        }
    }

    private void handleWithLine(String stationId, String lineString, Station station, Part part, boolean isOneWay) {
        List<String> linesConnected = getLinesConnectedOn(lineString, part);
        for (String line : linesConnected) {
            Station connectedStation = stations.get(new Long(stationId));
            String toEnum = getToEnum(line);
            Line lineEnum = Line.valueOf(toEnum);
            station.addConnection(lineEnum, connectedStation, isOneWay);
            LOGGER.log(Level.INFO, "Added connection to stationId {0} on line {1}",
                       new String[] {stationId, lineEnum.name()});
        }
    }

    private void handleNoLine(Station station, Part part, String[] connectionParts, boolean isOneWay) {
        String[] lines = part.getLines();
        String stationId = connectionParts[0];
        Station connectedStation = stations.get(new Long(stationId));
        String toEnum = getToEnum(lines[0]);
        Line lineEnum = Line.valueOf(toEnum);
        station.addConnection(lineEnum, connectedStation, isOneWay);
        LOGGER.log(Level.INFO, "Added connection to stationId {0} on line {1}",
                   new String[] {stationId, lineEnum.name()});
    }

    private void handleOneWay(String stationId, String lineString, Station station, Part part,
                              String[] connectionParts) {
        String tmp = lineString.toLowerCase().replace("one-way", "");
        LOGGER.log(Level.INFO, "ONE-WAY OCCURRENCE: " + tmp);
        boolean hasParts = !tmp.equals(")");
        if(hasParts) {
            handleWithLine(stationId, lineString, station, part, true);
        }
        else {
            handleNoLine(station, part, connectionParts, true);
        }
    }

    private String numToString(String num) {
        String tmp;
        switch (num) {
            case "1":
                tmp = "one";
                break;
            case "2":
                tmp = "two";
                break;
            case "3":
                tmp = "three";
                break;
            case "4":
                tmp = "four";
                break;
            case "5":
                tmp = "five";
                break;
            case "6":
                tmp = "six";
                break;
            case "7":
                tmp = "seven";
                break;
            case "8":
                tmp = "eight";
                break;
            case "9":
                tmp = "nine";
                break;
            default:
                tmp = "";
                break;
        }
        return tmp.toUpperCase();
    }

    private List<String> getLinesConnectedOn(String linesString, Part stationPart) {
        List<String> connectedLines = new ArrayList<>();
        String[] allLines = stationPart.getLines();
        if (linesString == null || "".compareTo(linesString) == 0) {
            connectedLines.add(allLines[0]);
        }
        else {
            if (linesString.contains("All")) {
                return Arrays.asList(allLines);
            }
            else {
                for (String line : allLines) {
                    if (linesString.contains(line)) {
                        connectedLines.add(line);
                    }
                }
            }
        }
        return connectedLines;
    }

    private String getToEnum(String line) {
        int endIdx = line.contains(" ") ? line.indexOf(" ") : line.length();
        return line.toUpperCase().substring(0, endIdx);
    }

    private void addAccessibility(Station station, Part part) {
        String[] access = part.getAccessibility();
        if(access.length > 0 && !access[0].toUpperCase().equals("N")) {
            if(access[0].toUpperCase().equals("Y")) {
                String line = station.getLines().get(0).getLineName();
                station.setAccessibleLines(new String[]{line});
            }
            else {
                station.setAccessibleLines(access);
            }
        }
    }

    private void addTerminus(Station station, Part part) {
        String[] term = part.getTerminus();
        if(term.length > 0 && !term[0].toUpperCase().equals("N")) {
            if(term[0].toUpperCase().equals("Y")) {
                String line = station.getLines().get(0).getLineName();
                station.setTerminusLines(new String[]{line});
            }
            else {
                station.setTerminusLines(term);
            }
        }
    }

}
