package org.oster.underground.utils;
public class Part {
    public final static int NUMBER_OF_PARTS = 7;
    private final static String DELIMITER = ";";

    private Long id;
    private String name;
    private String[] lines;
    private String[] zones;
    private String[] connections;
    private String[] accessibility;
    private String[] terminus;

    public Part() {

    }

    public Part(String id, String name, String lines, String zones, String connections, String accessibility,
                 String terminus) {
        this.id = new Long(id);
        this.name = name;
        this.lines = lines.trim().split(DELIMITER);
        this.zones = zones.trim().split(DELIMITER);
        this.connections = connections.trim().split(DELIMITER);
        this.accessibility = accessibility.trim().split(DELIMITER);
        this.terminus = terminus.trim().split(DELIMITER);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getLines() {
        return lines;
    }

    public void setLines(String[] lines) {
        this.lines = lines;
    }

    public String[] getZones() {
        return zones;
    }

    public void setZones(String[] zones) {
        this.zones = zones;
    }

    public String[] getConnections() {
        return connections;
    }

    public void setConnections(String[] connections) {
        this.connections = connections;
    }

    public String[] getAccessibility() {
        return accessibility;
    }

    public void setAccessibility(String[] accessibility) {
        this.accessibility = accessibility;
    }

    public String[] getTerminus() {
        return terminus;
    }

    public void setTerminus(String[] terminus) {
        this.terminus = terminus;
    }

}
