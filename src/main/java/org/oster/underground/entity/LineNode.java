package org.oster.underground.entity;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

@NodeEntity(label="Line")
public class LineNode extends AbstractNode {
    @GraphId
    private Long id;

    @Index(unique=true, primary=true)
    @Property(name="nodeId")
    private Long lineId;

    @Property
    private String name;

    public LineNode() {

    }

    public LineNode(Long lineId, String name) {
        this.lineId = lineId;
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLineId() {
        return lineId;
    }

    public void setLineId(Long lineId) {
        this.lineId = lineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("[ ")
           .append("graphId: ").append(id).append(", ")
           .append("lineId: ").append(lineId).append(", ")
           .append("lineName: ").append(name)
           .append(" ]");
        return buf.toString();
    }

}
