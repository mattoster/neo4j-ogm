package org.oster.underground.entity;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

@NodeEntity(label="Zone")
public class ZoneNode extends AbstractNode {
    @GraphId
    private Long id;

    @Index(unique=true, primary=true)
    @Property(name="nodeId")
    private Long zoneId;

    @Property
    private String name;

    public ZoneNode() {

    }

    public ZoneNode(Long zoneId, String name) {
        this.zoneId = zoneId;
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getZoneId() {
        return zoneId;
    }

    public void setZoneId(Long zoneId) {
        this.zoneId = zoneId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("[ ")
           .append("graphId: ").append(id).append(", ")
           .append("zoneId: ").append(zoneId).append(", ")
           .append("zoneName: ").append(name)
           .append(" ]");
        return buf.toString();
    }

}
