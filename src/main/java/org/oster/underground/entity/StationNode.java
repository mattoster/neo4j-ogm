package org.oster.underground.entity;

import java.util.ArrayList;
import java.util.List;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity(label = "Station")
public class StationNode extends AbstractNode {
    @GraphId
    private Long id;

    @Index(unique=true, primary=true)
    @Property(name="nodeId")
    private Long stationId;

    @Property
    private String name;

    @Property
    private String[] accessibleLines;

    @Property
    private String[] terminusLines;

    @Relationship(type="On")
    private List<LineNode> lines = new ArrayList<>();

    @Relationship(type="In")
    private List<ZoneNode> zones = new ArrayList<>();

    @Relationship(type="Connects")
    private List<Connection> connections = new ArrayList<>();

    public StationNode() {

    }

    public StationNode(Long stationId, String name) {
        this.stationId = stationId;
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getAccessibleLines() {
        return accessibleLines;
    }

    public void setAccessibleLines(String[] accessibleLines) {
        this.accessibleLines = accessibleLines;
    }

    public String[] getTerminusLines() {
        return terminusLines;
    }

    public void setTerminusLines(String[] terminusLines) {
        this.terminusLines = terminusLines;
    }

    public List<LineNode> getLines() {
        return lines;
    }

    public void setLines(List<LineNode> lines) {
        this.lines = lines;
    }

    public List<ZoneNode> getZones() {
        return zones;
    }

    public void setZones(List<ZoneNode> zones) {
        this.zones = zones;
    }

    public void addConnection(Connection connection) {
        if (this.connections == null) {
            this.connections = new ArrayList<>();
        }
        this.connections.add(connection);
    }

    public List<Connection> getConnections() {
        return connections;
    }

    public void setConnections(List<Connection> connections) {
        this.connections = connections;
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("[ ")
           .append("graphId: ").append(id).append(", ")
           .append("stationId: ").append(stationId).append(", ")
           .append("stationName: ").append(name)
           .append(" ]");
        return buf.toString();
    }

}
