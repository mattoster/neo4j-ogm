package org.oster.underground.entity;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type="Connects")
public class Connection {
    @GraphId
    Long id;

    @StartNode
    StationNode startNode;

    @EndNode
    StationNode endNode;

    @Property(name="line")
    String connectionLine;

    public Connection(StationNode startNode, StationNode endNode, String connectionLine) {
        this.startNode = startNode;
        this.endNode = endNode;
        this.connectionLine = connectionLine;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public StationNode getStartNode() {
        return startNode;
    }

    public void setStartNode(StationNode startNode) {
        this.startNode = startNode;
    }

    public StationNode getEndNode() {
        return endNode;
    }

    public void setEndNode(StationNode endNode) {
        this.endNode = endNode;
    }

    public String getConnectionLine() {
        return connectionLine;
    }

    public void setConnectionLine(String connectionLine) {
        this.connectionLine = connectionLine;
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("[ ")
           .append("startId: ").append(startNode.getStationId()).append(", ")
           .append("endId: ").append(endNode.getStationId()).append(", ")
           .append("lineName: ").append(connectionLine)
           .append(" ]");
        return buf.toString();
    }

}
