package org.oster.underground.entity;
public abstract class AbstractNode implements Node {

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getId() == null || getClass() != o.getClass()) {
            return false;
        }

        Node node = (Node) o;

        if (!getId().equals(node.getId())) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (getId() == null) ? -1 : getId().hashCode();
    }

}
