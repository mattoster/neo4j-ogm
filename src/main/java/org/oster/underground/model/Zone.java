package org.oster.underground.model;

import java.util.HashMap;
import java.util.Map;

public enum Zone {
    ONE(1L, "1"),
    TWO(2L, "2"),
    THREE(3L, "3"),
    FOUR(4L, "4"),
    FIVE(5L, "5"),
    SIX(6L, "6"),
    SEVEN(7L, "7"),
    EIGHT(8L, "8"),
    NINE(9L, "9");

    private final Long zoneId;
    private final String zoneName;
    private final Map<String, Zone> zonesByName = new HashMap<>();

    Zone(Long zoneId, String zoneName) {
        this.zoneId = zoneId;
        this.zoneName = zoneName;
        this.zonesByName.put(this.zoneName, this);
    }

    public Long getZoneId() {
        return this.zoneId;
    }

    public String getZoneName() {
        return this.zoneName;
    }
    
    public Zone getZoneEnumByZoneName(String zoneName) {
        return this.zonesByName.get(zoneName);
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("[ ")
           .append("zoneId: ").append(zoneId).append(", ")
           .append("zoneName: ").append(zoneName)
           .append(" ]");
        return buf.toString();
    }

}
