package org.oster.underground.model;
import java.util.Collections;
import java.util.Map;
import org.apache.commons.collections4.map.HashedMap;

public enum Line {
    BAKERLOO(1L, "Bakerloo"),
    CENTRAL(2L, "Central"),
    CIRCLE(3L, "Circle"),
    DISTRICT(4L, "District"),
    HAMMERSMITH(5L, "Hammersmith & City"),
    JUBILEE(6L, "Jubilee"),
    METROPOLITAN(7L, "Metropolitan"),
    NORTHERN(8L, "Northern"),
    PICCADILLY(9L, "Piccadilly"),
    VICTORIA(10L, "Victoria"),
    WATERLOO(11L, "Waterloo & City");

    private static final Map<String, Line> linesByName;// = new HashMap<>();

    private final Long lineId;
    private final String lineName;

    static {
        Map<String, Line> tmp = new HashedMap<>();
        for (Line line : Line.values()) {
            tmp.put(line.getLineName(), line);
        }
        linesByName = Collections.unmodifiableMap(tmp);
    }

    private Line(Long lineId, String lineName) {
        this.lineId = lineId;
        this.lineName = lineName;
    }

    public Long getLineId() {
        return this.lineId;
    }

    public String getLineName() {
        return this.lineName;
    }

    public static Line getLineEnumByLineName(String lineName) {
        return linesByName.get(lineName);
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("[ ")
           .append("lineId: ").append(lineId).append(", ")
           .append("lineName: ").append(lineName)
           .append(" ]");
        return buf.toString();
    }

}
