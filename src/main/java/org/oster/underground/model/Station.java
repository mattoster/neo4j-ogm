package org.oster.underground.model;
import java.util.ArrayList;
import java.util.List;

public class Station {
    private final Long id;
    private String name;

    private String[] accessibleLines;
    private String[] terminusLines;

    private List<Line> lines = new ArrayList<>();
    private List<Zone> zones = new ArrayList<>();

    private List<Conn> connections = new ArrayList<>();

    public Station(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return this.id;
    }

    public void addLine(Line line) {
        this.lines.add(line);
    }

    public void addZone(Zone zone) {
        this.zones.add(zone);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Line> getLines() {
        return lines;
    }

    public List<Zone> getZones() {
        return zones;
    }

    public List<Conn> getConnections() {
        return this.connections;
    }

    public void addConnection(Line line, Station station, boolean isOneWay) {
        Conn connection = new Conn(line, station, isOneWay);
        connections.add(connection);
    }

    public String[] getAccessibleLines() {
        return accessibleLines;
    }

    public void setAccessibleLines(String[] accessibleLines) {
        this.accessibleLines = accessibleLines;
    }

    public String[] getTerminusLines() {
        return terminusLines;
    }

    public void setTerminusLines(String[] terminusLines) {
        this.terminusLines = terminusLines;
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("[ ")
           .append("stationId: ").append(id).append(", ")
           .append("stationName: ").append(name)
           .append(" ]");
        return buf.toString();
    }

}
