package org.oster.underground.model;

public class Conn {
    private Line line;
    private Station station;
    private boolean isOneWay;

    public Conn(Line line, Station station, boolean isOneWay) {
        this.line = line;
        this.station = station;
        this.isOneWay = isOneWay;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public boolean isOneWay() {
        return isOneWay;
    }

    public void setOneWay(boolean oneWay) {
        isOneWay = oneWay;
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("[ ")
           .append("connLineName: ").append(line.getLineName()).append(", ")
           .append("connStationId: ").append(station.getId()).append(", ")
           .append(" ]");
        return buf.toString();
    }

}
