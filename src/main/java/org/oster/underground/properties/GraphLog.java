package org.oster.underground.properties;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GraphLog {
    private static final Logger LOGGER = Logger.getLogger(GraphLog.class.getName());
    
    private static Level loggerLevel = Level.WARNING;
    private static Properties loggerProps = new Properties();
    
    static {
        // Set logger to default level
        LOGGER.setLevel(loggerLevel);
        
        loggerProps = GraphProps.getLoggingProperties();
        setLoggingLevelFromProperty(loggerProps.getProperty("level"));

        // Set logger to property level
        LOGGER.setLevel(loggerLevel);
    }
    
    @SuppressWarnings("rawtypes")
    public static Logger getLogger(Class classIn) {
        Logger logger = Logger.getLogger(classIn.getName());
        logger.setLevel(loggerLevel);
        return logger;
    }
    
    private static void setLoggingLevelFromProperty(String propLevel) {
        if(propLevel != null) {
            switch(propLevel.toLowerCase()) {
                case "info":
                    loggerLevel = Level.INFO;
                    break;
                default:
                    LOGGER.log(Level.INFO, "Logger level left at: {0}", loggerLevel);
            }
        }
        else {
            LOGGER.log(loggerLevel, "Could not find logging level in properties, level set to: {0}", loggerLevel);
        }
    }
    
}
