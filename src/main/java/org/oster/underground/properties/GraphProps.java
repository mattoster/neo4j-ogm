package org.oster.underground.properties;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GraphProps {
    private static final Logger LOGGER = Logger.getLogger(GraphProps.class.getName());

    private static final String APP_PROP_FILE = "src/main/resources//app.properties";
    private static final String LOG_PROP_FILE = "src/main/resources//logging.properties";

    private static Properties appProps = new Properties();
    private static Properties logProps = new Properties();

    static {
        readApplicationProps();
        readLoggingProps();
    }
    
    public static Properties getApplicationProperties() {
        return appProps;
    }
    
    public static String getApplicationProperty(String property) {
        return appProps.getProperty(property);
    }
    
    public static Properties getLoggingProperties() {
        return logProps;
    }
    
    public static String getLoggingProperty(String property) {
        return logProps.getProperty(property);
    }
    
    private static void readApplicationProps() {
        try(Reader reader = Files.newBufferedReader(Paths.get(APP_PROP_FILE))) {
            appProps.load(reader);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error reading in application properties from: {0}", APP_PROP_FILE);
        }
    }
    
    private static void readLoggingProps() {
        try(Reader reader = Files.newBufferedReader(Paths.get(LOG_PROP_FILE))) {
            logProps.load(reader);
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Error reading in logging properties from: {0}", LOG_PROP_FILE);
        }
    }

}
